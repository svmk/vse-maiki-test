<?php

/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Укротитель ссылок';
?>
<style>
    #short-btn {
        padding: 2px 6px;
    }
    #redirecturl-url {
        width: 100%;
    }
    .field-redirecturl-url {
        width: 80%;
    }
</style>
<div class="site-index">
    <?php Pjax::begin(); ?>
    <div id="short-url-form" class="jumbotron">
        <h1>Укротитель ссылок</h1>

        <?php 
            if ($shortCode) { 
                $url = Url::to(['redirect','code' => $shortCode,],true);
        ?>
            <p>
                Короткая ссылка: <input readonly type="text" id="short-link" value="<?php 
                    echo Html::encode($url);
                ?>">
            </p>
            <p>
                Открыть:
                <?php echo Html::a($url,$url,['target' => '_blank',]); ?>
            </p>
            <p>
                <?php echo Html::a('Укоротить еще одну ссылку',Url::to(['index'])); ?>
            </p>
        <?php } else { ?>
            <?php $form = ActiveForm::begin(['layout' => 'inline',]); ?>
            <p class="lead">Введите ссылку, которую вы хотите укоротить.</p>
            
            <p>
                <?php echo $form->field(
                $model,
                'url',
                [
                    'template' => "{input}\n{error}",
                ]
                )->label(
                    false
                )->textInput([
                    'placeholder' => 'http://',
                ]); ?>
                <button id="short-btn" type="submit" class="btn btn-success">Укоротить</button>
            </p>
            <?php ActiveForm::end(); ?>
        <?php } ?>        
    </div>
    <?php Pjax::end(); ?>
</div>
<?php 
$this->registerJs("
    $('#redirecturl-url').focus();
    $(document).delegate('#short-link','focus',function() {
       $(this).select();
    });
");