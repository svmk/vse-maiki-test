<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_redirect_urls`.
 */
class m160614_171958_create_table_redirect_urls extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('redirect_urls', [
            'id' => $this->primaryKey(),
            'url'       => $this->text(),
        ]);        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('redirect_urls');
    }
}
