<?php

namespace tests\codeception\unit\models;
use app\models\RedirectUrl;
use yii\codeception\TestCase;

class RedirectUrlTest extends TestCase
{
    public function testNumToCode() {
    	$this->assertTrue(
			RedirectUrl::numToCode(0) == 'a'
		);
		$this->assertTrue(
			RedirectUrl::numToCode(1) == 'c'
		);
		$this->assertTrue(
			RedirectUrl::numToCode(10) == 't'
		);
		$this->assertTrue(
			RedirectUrl::numToCode(22) == '9'
		);
		$this->assertTrue(
			RedirectUrl::numToCode(23) == 'ca'
		);
		$this->assertTrue(
			RedirectUrl::numToCode(24) == 'cc'
		);
		$this->assertTrue(
			RedirectUrl::numToCode(25) == 'ce'
		);
    }

    public function testcodeToNum() {
		$this->assertTrue(
			RedirectUrl::codeToNum('a') == '0'
		);
		$this->assertTrue(
			RedirectUrl::codeToNum('c') == '1'
		);
		$this->assertTrue(
			RedirectUrl::codeToNum('t') == '10'
		);
		$this->assertTrue(
			RedirectUrl::codeToNum('9') == '22'
		);
		$this->assertTrue(
			RedirectUrl::codeToNum('ca') == '23'
		);
		$this->assertTrue(
			RedirectUrl::codeToNum('cc') == '24'
		);
		$this->assertTrue(
			RedirectUrl::codeToNum('ce') == '25'
		);    	
    }
}
