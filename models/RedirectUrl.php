<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "redirect_urls".
 *
 * @property integer $id
 * @property string $url
 */
class RedirectUrl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'redirect_urls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'url'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
        ];
    }

    /**
     * @inheritdoc
     * @return RedirectUrlQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RedirectUrlQuery(get_called_class());
    }

    /**
     * getShortCode возвращает сокращённый код
     * 
     * @return string
     */
    public function getShortCode() {
        return self::numToCode($this->getPrimaryKey());
    }

    /**
     * alphabet возвращает алфавит 
     * 
     * @return string
     */
    protected static function alphabet() {
        return 'aceghkmnostuxyz23456789';
    }

    /**
     * numToCode кодирует положительные числа
     * 
     * @param integer $num число
     *
     * @return string|null
     */
    public static function numToCode($num) {
        if ($num < 0) {
            return null;
        }
        $result = '';
        $alphabet = self::alphabet();
        $base     = strlen($alphabet);
        do {
            $digit  = $num % $base;
            $num    = floor($num / $base);
            $result = $alphabet[$digit].$result;            
        } while ($num > 0);
        return $result;
    }

    /**
     * codeToNum кодирует строку в число
     * 
     * @param string $code код
     *
     * @return integer|null
     */
    public static function codeToNum($code) {
        $code = strtolower($code);
        $alphabet = self::alphabet();
        $base     = strlen($alphabet);
        $len = strlen($code);
        $result = 0;
        for ($i = 0; $i < $len; $i++) {
            $digitBase = pow($base,$len - $i - 1);
            $digitValue = strpos($alphabet,$code[$i]);
            if ($digitValue === false) {
                return null;
            }
            $result = $result + $digitValue * $digitBase;            
        }
        return $result;
    }
}
