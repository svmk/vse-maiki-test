<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RedirectUrl]].
 *
 * @see RedirectUrl
 */
class RedirectUrlQuery extends \yii\db\ActiveQuery
{
    /**
     * thatHaveShortCode фильтрует по короткому коду
     * 
     * @param string $code код
     *
     * @return this
     */
    public function thatHaveShortCode($code) {
        return $this->andWhere(['id' => RedirectUrl::codeToNum($code),]);
    }
}
