Тестовое задание ВсеМайки.ру
============================

Тестовое задание выполнено на базе [Yii-фреймворка](http://www.yiiframework.com/) второй версии.

Требования перед установкой
---------------------------

* PHP 5.4
* Apache с .htaccess


Установка
---------

### Установите зависимости используя composer

Если у вас нет [Composer](http://getcomposer.org/), вы должны установить его [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

После чего выполните команды из корневого каталога проекта

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar install --no-dev
~~~

### Настройте подключение к БД

Отредактируйте файл `config/db.php` с настоящими настройками.

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=vse-maiki-test',
    'username' => 'vse-maiki-test',
    'password' => 'vse-maiki-test',
    'charset' => 'utf8',
];
```

### Выполните миграции

Из каталога проекта выполните команду
```
./yii migrate/up
```

После выполнения всех шагов можете открыть страницу

### Включите mod_rewrite для apache
```
a2enmod rewrite
```

### Включите mod_php для apache
```
a2enmod php5
```

### Создайте виртуальный хост Apache
Создайте виртуальный хост для Apache

```apache
<VirtualHost *:80>  
  ServerName localhost.vse-maiki-test
  ServerAdmin webmaster@localhost
  DocumentRoot /var/www/vse-maiki-test/web
  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined  
</VirtualHost>
<Directory "/var/www/vse-maiki-test/web">
    Options +Includes +FollowSymLinks 
    AllowOverride All
</Directory>
```
