<?php
namespace app\routes;
use yii\web\UrlRuleInterface;
use yii\base\Object;
use yii\base\InvalidConfigException;
class RedirectRoute extends Object implements UrlRuleInterface{
    /**
     * $urlPrefix URL префикс
     * @var string
     */
	public $urlPrefix;

    /**
     * $route маршрут до действия
     * @var string
     */
	public $route;

    /**
     * $param название параметра для действия
     * @var string
     */
	public $param;

	/**
     * Parses the given request and returns the corresponding route and parameters.
     * @param UrlManager $manager the URL manager
     * @param Request $request the request component
     * @return array|boolean the parsing result. The route and the parameters are returned as an array.
     * If false, it means this rule cannot be used to parse this path info.
     */
    public function parseRequest($manager, $request) {
    	$path = $request->getPathInfo();
    	$urlPrefixLength = strlen($this->urlPrefix);
    	if (substr($path,0,$urlPrefixLength) == $this->urlPrefix) {
    		$code = substr($path,$urlPrefixLength);
    		if ($code) {
	    		return [$this->route,[$this->param => $code]];    			
    		}
    	}
    	return false;
    }

    /**
     * Creates a URL according to the given route and parameters.
     * @param UrlManager $manager the URL manager
     * @param string $route the route. It should not have slashes at the beginning or the end.
     * @param array $params the parameters
     * @return string|boolean the created URL, or false if this rule cannot be used for creating this URL.
     */
    public function createUrl($manager, $route, $params) {
    	if ($route == $this->route) {
    		if (isset($params[$this->param])) {
    			$code = $params[$this->param];
    			$url = $this->urlPrefix.$code;
    			unset($params[$this->param]);
    			$query = http_build_query($params);
    			if ($query) {
    				$url = $url.'?'.$query;
    			}
    			return $url;
    		}
    	}
    	return false;
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
 		if ($this->urlPrefix === null) {
 			throw new InvalidConfigException('Не установлен параметр urlPrefix');
 		}
		if ($this->route === null) {
 			throw new InvalidConfigException('Не установлен параметр route');	
 		}
		if ($this->param === null) {
 			throw new InvalidConfigException('Не установлен параметр param');		
 		}
    }
}