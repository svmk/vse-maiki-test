<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RedirectUrl;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * actionIndex выводит главную страницу
     * 
     * @return string
     */
    public function actionIndex()
    {
        $model = new RedirectUrl;
        $shortCode = null;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $shortCode = $model->getShortCode();
        }
        return $this->render(
            'index',
            [
                'model' => $model,
                'shortCode' => $shortCode,
            ]
        );
    } 

    public function actionRedirect($code) {
        $model = RedirectUrl::find(
        )->thatHaveShortCode(
            $code
        )->one();
        if ($model === null) {
            throw new NotFoundHttpException('Ссылка не найдена');
        }
        return $this->redirect($model->url);
    }
}
